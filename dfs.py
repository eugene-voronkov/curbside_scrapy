import json
file_path = 'state_space.jl'
#file_path = 'curbside_scrapy/spiders/curbside10.jl'
lut = {}

with open(file_path) as f:
    for line in f.readlines():
        data = json.loads(line)
        lut[data['id']] = data

root_node = None
with open(file_path) as f:
    root_node = lut[json.loads(f.readline())['id']]

def secret_search(current_node):
    if 'secret' in current_node and current_node['secret'] != u'':
        print current_node['secret'],

    if 'next' in current_node:
        try:
            if isinstance(current_node['next'], list):
                for key in current_node['next']:
                    secret_search(lut[key])
            else:
                secret_search(lut[current_node['next']])
        except:
            pass



secret_search(root_node)