# -*- coding: utf-8 -*-
import scrapy
import json
import requests
from scrapy.http.request import Request

class PuzzleCrawlerSpider(scrapy.Spider):
    name = "puzzle_crawler"

    def start_requests(self):
        yield Request('http://challenge.curbside.com/start',self.parse, headers=self.session_header)

    #Ended up not using
    def session_expiring_soon(self):
        return 10 > (dateutil.parser.parse(self.session_header['expire_at']).replace(tzinfo=None) - datetime.datetime.utcnow()).total_seconds()

    def update_header(self):
        self.session_header = json.loads(requests.get('http://challenge.curbside.com/get-session').text)

    def __init__(self, *a, **kw):
        super(PuzzleCrawlerSpider, self).__init__(*a, **kw)
        self.update_header()

    #prioritize timed out request retries in queue
    #Never shows up in logger but seem to get less missing letters
    #then default retry middleware with this enabled.
    def errback_timeout(self, failure):
        if failure.check(TimeoutError, TCPTimedOutError):
            request = failure.request
            self.logger.info('TimeoutError on %s', request.url)
            yield Request('http://challenge.curbside.com/'+key,self.parse, headers=self.session_header, priority=10)

    def parse(self, response, errback=errback_timeout):
        current_node = json.loads(response.text)
        yield current_node
        if 'next' in current_node:
            if isinstance(current_node['next'], list):
                for key in current_node['next']:
                    yield Request('http://challenge.curbside.com/'+key,self.parse, headers=self.session_header)
            else:
                yield Request('http://challenge.curbside.com/'+current_node['next'],self.parse, headers=self.session_header)