## About Me

------

### Why would I be a good fit?
I can program at every level of the web stack.  Based on the positions you're hiring for it looks like that might be of value at Curbside.

### Buzzword Soup
* Python, NodeJS/Javascript, C, Bash, Java, CSS, HTML, Algorithms
* Linux, FreeBSD, Postgres, MSSQL, Django, Flask, Express
* Many more at or above fizzbuzz level.

### Contact Info
* e-mail: eugene.voronkov at gmail.com
* number: 404 993 2241
  
  
  
  
## Code Summary

------

id of json requests seems to change on authorization expiration.  Setting Scrapy's concurrency level to 32 with crawl depth of 10 seems to capture important part of state space before headers are able to expire.

Reconstructing message required depth first search for states with non-empty secret fields.  dfs.py returns secret message inside the tree.

Sometimes there are letters missing.  Not sure if this bug is on my end or yours.  I'm making sure timed out requests are fetched again but nothing is showing up in the logs to indicate this is needed.  If I was more invested in this project I would look for bugs in how the crawler is choosing children nodes and I would look into doing sanity checks on fetched data(check for id and secret fields) to see if I can pinpoint the problem.


## Example Output

------

To search secret on pre-crawled data.
```bash
gene@gene-VirtualBox:~/curbside_scrapy$ python dfs.py
A p p l y   a t   s y s t e m d @ s h o p c u r b s i d e . c o m   w i t h   y o u r   c o d e
```

To crawl data first.
```bash
gene@gene-VirtualBox:~/curbside_scrapy$ ./run.sh
2017-04-06 17:33:41 [scrapy] INFO: Scrapy 1.1.1 started (bot: curbside_scrapy)
2017-04-06 17:33:41 [scrapy] INFO: Overridden settings: {'NEWSPIDER_MODULE': 'curbside_scrapy.spiders', 'FEED_URI': 'state_space.jl', 'LOG_LEVEL': 'INFO', 'CONCURRENT_REQUESTS': 32, 'SPIDER_MODULES': ['curbside_scrapy.spiders'], 'BOT_NAME': 'curbside_scrapy', 'DOWNLOAD_TIMEOUT': 10, 'FEED_FORMAT': 'jl'}
2017-04-06 17:33:41 [scrapy] INFO: Enabled extensions:
['scrapy.extensions.feedexport.FeedExporter',
 'scrapy.extensions.logstats.LogStats',
 'scrapy.extensions.telnet.TelnetConsole',
 'scrapy.extensions.corestats.CoreStats']
2017-04-06 17:33:41 [scrapy] INFO: Enabled downloader middlewares:
['scrapy.downloadermiddlewares.httpauth.HttpAuthMiddleware',
 'scrapy.downloadermiddlewares.downloadtimeout.DownloadTimeoutMiddleware',
 'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware',
 'scrapy.downloadermiddlewares.retry.RetryMiddleware',
 'scrapy.downloadermiddlewares.defaultheaders.DefaultHeadersMiddleware',
 'scrapy.downloadermiddlewares.redirect.MetaRefreshMiddleware',
 'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware',
 'scrapy.downloadermiddlewares.redirect.RedirectMiddleware',
 'scrapy.downloadermiddlewares.cookies.CookiesMiddleware',
 'scrapy.downloadermiddlewares.chunked.ChunkedTransferMiddleware',
 'scrapy.downloadermiddlewares.stats.DownloaderStats']
2017-04-06 17:33:41 [scrapy] INFO: Enabled spider middlewares:
['scrapy.spidermiddlewares.httperror.HttpErrorMiddleware',
 'scrapy.spidermiddlewares.offsite.OffsiteMiddleware',
 'scrapy.spidermiddlewares.referer.RefererMiddleware',
 'scrapy.spidermiddlewares.urllength.UrlLengthMiddleware',
 'scrapy.spidermiddlewares.depth.DepthMiddleware']
2017-04-06 17:33:41 [scrapy] INFO: Enabled item pipelines:
[]
2017-04-06 17:33:41 [scrapy] INFO: Spider opened
2017-04-06 17:33:41 [scrapy] INFO: Crawled 0 pages (at 0 pages/min), scraped 0 items (at 0 items/min)
2017-04-06 17:34:42 [scrapy] INFO: Crawled 1425 pages (at 1425 pages/min), scraped 1416 items (at 1416 items/min)
2017-04-06 17:35:41 [scrapy] INFO: Crawled 3014 pages (at 1589 pages/min), scraped 3005 items (at 1589 items/min)
2017-04-06 17:36:41 [scrapy] INFO: Crawled 4563 pages (at 1549 pages/min), scraped 4555 items (at 1550 items/min)
2017-04-06 17:37:42 [scrapy] INFO: Crawled 6356 pages (at 1793 pages/min), scraped 6347 items (at 1792 items/min)
2017-04-06 17:38:42 [scrapy] INFO: Crawled 7851 pages (at 1495 pages/min), scraped 7843 items (at 1496 items/min)
2017-04-06 17:38:46 [scrapy] INFO: Closing spider (finished)
2017-04-06 17:38:46 [scrapy] INFO: Stored jl feed (7845 items) in: state_space.jl
2017-04-06 17:38:46 [scrapy] INFO: Dumping Scrapy stats:
{'downloader/request_bytes': 3275105,
 'downloader/request_count': 7969,
 'downloader/request_method_count/GET': 7969,
 'downloader/response_bytes': 2509484,
 'downloader/response_count': 7969,
 'downloader/response_status_count/200': 7845,
 'downloader/response_status_count/400': 124,
 'finish_reason': 'finished',
 'finish_time': datetime.datetime(2017, 4, 6, 21, 38, 46, 278228),
 'item_scraped_count': 7845,
 'log_count/INFO': 13,
 'request_depth_max': 10,
 'response_received_count': 7969,
 'scheduler/dequeued': 7969,
 'scheduler/dequeued/memory': 7969,
 'scheduler/enqueued': 7969,
 'scheduler/enqueued/memory': 7969,
 'start_time': datetime.datetime(2017, 4, 6, 21, 33, 41, 891856)}
2017-04-06 17:38:46 [scrapy] INFO: Spider closed (finished)
A p p l y   a t   s y s t e m d @ s h o p c u r b s i d e . c o m   w i t h   y o u r   c o d e
```